OSP's Collaboration Agreement
=============================

This document has moved to https://gitlab.constantvzw.org/osp/osp.meta.association/tree/master/association-4-collaboration-agreement

- - -

A document to inform our future collaborators about the way we work.

TO make things easier, the document is edited on an etherpad.
To download the pad as an HTML file and to insert CSS, you can run in your terminal:
    bash collab-agreement.sh

It will update the file `source/collab-agreement.html`.
You can then open it in Chromium and export it as a PDF (made for an A5 8 pages booklet).
